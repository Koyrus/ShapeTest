﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2 {
    
    using Type = Types.FigureType;
    //Тип struct представляет собой тип значения, который обычно используется для инкапсуляции небольших групп связанных переменных
    struct Types {
        //Ключевое слово enum используется для объявления перечисления — отдельного типа, который состоит из набора именованных констант, называемого списком перечислителей.
        public enum FigureType { Circle, Square, Rhombus, Triangle }
    }

    class Figure {

        // Get Определяет метод доступа в свойстве или индексаторе.Задание или извлечение значения в закрытом поле
        public Type Type { get;set; }
        public int Corners { get; set; }
        public bool IsRound { get; set; }
    }

   
    class Square : Figure {
        public Square() {
            Corners = 4;
            IsRound = false;
            Type = Type.Square;
        }
    }
    class Rhombus : Figure {
        public Rhombus() {
            Corners = 4;
            IsRound = false;
            Type = Type.Rhombus;
        }
    }
    class Triangle : Figure {
        public Triangle() {
            Corners = 3;
            IsRound = false;
            Type = Type.Triangle;
        }
    }
    class Circle : Figure {
        public Circle() {
            Corners = 0;
            IsRound = true;
            Type = Type.Circle;
        }
    }

    class Shape {
        static void Main(string[] args){
            Figure[] figures = new Figure[4];
            figures[0] = new Circle();
            figures[1] = new Square();
            figures[2] = new Rhombus();
            figures[3] = new Triangle();

            //int corners = 0;
            //int straight = 0;
            //int radius = 0;
            //int top = 0;

            for (int i = 0;i<figures.Length;i++) {
                Console.WriteLine(figures[i].Type +" "+ figures[i].Corners);
            }

            //foreach (Figure f in figures) {
            //    switch (f.Type) {
            //        case Type.Circle: corners++; break;
            //        case Type.Rhombus: radius++; break;
            //        case Type.Square: straight++; break;
            //        case Type.Triangle: top++; break;
            //    }
            //}
            //Console.WriteLine("Круг: " + "\tУглы: " + new Circle().Corners);
            //Console.WriteLine("Квадрат: " + "\tПрямые: " + new Square().Corners);
            //Console.WriteLine("Ромб: "  + "\tПлоскости: " + new Rhombus().Corners);
            //Console.WriteLine("Треугольник: "  + "\tВершины: " + new Triangle().Corners);
            Console.ReadLine();
        }
      
     
}
}
